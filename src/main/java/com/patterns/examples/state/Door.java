package com.patterns.examples.state;

public class Door {

    private State state;

    public State getState() { return state; }
    public void setState(State state) { this.state = state; }

    public Door(State state) { this.state = state; }

    public void changeState() {
        if (this.getState() instanceof Open) {
            this.setState(new Close());
        } else if (this.getState() instanceof Close) {
            this.setState(new Open());
        }
    }

    public void move() { this.getState().openClose(); }
}
